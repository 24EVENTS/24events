package com.maler.dieck.android.fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.actionbarsherlock.app.SherlockFragment;
import com.maler.dieck.android.R;
import com.maler.dieck.android.helper.Helper;

public class ImageFragment extends SherlockFragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.image, container, false);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		WebView webView = (WebView) view.findViewById(R.id.webview);
		try {
			webView.loadDataWithBaseURL(
					null,
					Helper.readToEnd(
							getResources().getAssets().open("image.html"))
							.replace("##FILE##",
									getArguments().getString("file")),
					"text/html", "UTF-8", null);
		} catch (Exception e) {
			Log.e(getClass().getSimpleName(), "Error loading image", e);
		}
	}

}

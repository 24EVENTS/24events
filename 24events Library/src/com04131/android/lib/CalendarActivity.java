package com04131.android.lib;

import java.net.URLDecoder;
import java.text.SimpleDateFormat;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

public class CalendarActivity extends Activity {

	private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat(
			"yyyyMMdd'T'hhmmss'Z'");

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Log.d(getPackageName(), "URI: " + getIntent().getData());

		String summary = URLDecoder.decode(getIntent().getData()
				.getQueryParameter("summary"));
		String location = URLDecoder.decode(getIntent().getData()
				.getQueryParameter("location"));
		String description = URLDecoder.decode(getIntent().getData()
				.getQueryParameter("description"));
		String start = getIntent().getData().getQueryParameter("start");
		String end = getIntent().getData().getQueryParameter("end");

		Log.d(getPackageName(), "Summary: " + summary);
		Log.d(getPackageName(), "Location: " + location);
		Log.d(getPackageName(), "Description: " + description);
		Log.d(getPackageName(), "Start: " + start);
		Log.d(getPackageName(), "End: " + end);

		Intent intent = new Intent(Intent.ACTION_EDIT);
		intent.setType("vnd.android.cursor.item/event");
		intent.putExtra("title", summary);
		intent.putExtra("eventLocation", location);
		intent.putExtra("description", description);
		try {
			intent.putExtra("beginTime", DATE_FORMAT.parse(start).getTime());
		} catch (Exception e) {
			Log.w(getPackageName(), "Error parsing start: " + start, e);
		}

		try {
			intent.putExtra("endTime", DATE_FORMAT.parse(end).getTime());
		} catch (Exception e) {
			Log.w(getPackageName(), "Error parsing end: " + end, e);
		}
		startActivity(intent);
		finish();
	}

}

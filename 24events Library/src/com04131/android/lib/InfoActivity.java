package com04131.android.lib;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebView;

public class InfoActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.info);
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					getAssets().open("info.html")));
			StringBuilder builder = new StringBuilder(8192);
			String line;
			while ((line = reader.readLine()) != null) {
				builder.append(line);
			}
			reader.close();
			((WebView) findViewById(R.id.webview))
					.loadUrl("file:///android_asset/info.html");
		} catch (Exception e) {
			Log.e(getPackageName(), "Error loading info text", e);
		}
	}
}

package com04131.android.lib.fragments.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import com.actionbarsherlock.app.SherlockDialogFragment;
import com04131.android.lib.Billing;
import com04131.android.lib.IABActivity;
import com04131.android.lib.R;

import java.util.ArrayList;
import java.util.List;

/**
 * User: Goddchen
 * Date: 09.02.13
 */
public class RemoveAdsIAPDialogFragment extends SherlockDialogFragment {

    public static RemoveAdsIAPDialogFragment newInstance() {
        return new RemoveAdsIAPDialogFragment();
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final View view = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_iap, null);
        final ListView listView = (ListView) view.findViewById(R.id.list);
        listView.setAdapter(new ChoicesAdapter(getActivity(), getChoices()));
        return new AlertDialog.Builder(getActivity()).setView(view).setPositiveButton(android.R.string.ok,
                new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (listView.getCheckedItemPosition() != AdapterView.INVALID_POSITION) {
                    Choice selectedChoice = (Choice) listView.getAdapter().getItem(listView.getCheckedItemPosition());
                    ((IABActivity) getActivity()).iabHelper.launchPurchaseFlow(getActivity(), selectedChoice.key, 0,
                            ((IABActivity) getActivity()));
                }
            }
        }).setNegativeButton(android.R.string.cancel, null).setIcon(R.drawable.ic_dialog_remove_ads).setTitle(R
                .string.iap_title).create();
    }

    private List<Choice> getChoices() {
        List<Choice> choices = new ArrayList<Choice>();
        choices.add(new Choice(Billing.IAB_REMOVE_ADS_LEVEL_1, getString(R.string.remove_ads_1)));
        choices.add(new Choice(Billing.IAB_REMOVE_ADS_LEVEL_2, getString(R.string.remove_ads_2)));
        choices.add(new Choice(Billing.IAB_REMOVE_ADS_LEVEL_3, getString(R.string.remove_ads_3)));
        choices.add(new Choice(Billing.IAB_REMOVE_ADS_LEVEL_4, getString(R.string.remove_ads_4)));
        choices.add(new Choice(Billing.IAB_REMOVE_ADS_LEVEL_5, getString(R.string.remove_ads_5)));
        choices.add(new Choice(Billing.IAB_REMOVE_ADS_LEVEL_6, getString(R.string.remove_ads_6)));
        return choices;
    }

    private static class Choice {
        public String key, title;

        private Choice(String key, String title) {
            this.key = key;
            this.title = title;
        }
    }

    private class ChoicesAdapter extends ArrayAdapter<Choice> {

        public ChoicesAdapter(Context context, List<Choice> objects) {
            super(context, android.R.layout.simple_list_item_single_choice, objects);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = super.getView(position, convertView, parent);
            ((android.widget.TextView) view.findViewById(android.R.id.text1)).setText(getItem(position).title);
            return view;
        }
    }

}
